package storage

import (
	"context"
	"lms_service/models"
)

type StorageI interface {
	OTP() OTPRepoI
	User() UserRepoI
	Course() CourseRepoI
	Module() ModuleRepoI
	Lesson() LessonRepoI
	Group() GroupRepoI
}
type UserRepoI interface {
	Create(ctx context.Context, req *models.CreateUser) (*models.User, error)
	GetByID(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error)
	Update(ctx context.Context, req *models.UpdateUser) (*models.User, error)
	GetByEmail(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error)
	GetList(ctx context.Context, req *models.GetListUserRequest) (*models.GetListUserResponse, error)
	Delete(ctx context.Context, req *models.UserPrimaryKey) error
}


type OTPRepoI interface {
	Create(ctx context.Context, req *models.ForgetPasswords) error
	GetByEmail(ctx context.Context, req *models.ForgetPasswordPrimaryKey) (*models.Forget, error)
	Delete(ctx context.Context, req *models.ForgetPasswordPrimaryKey) error
}


type CourseRepoI interface {
	Create(ctx context.Context, req *models.CreateCourse) (*models.Course, error)
	GetByID(ctx context.Context, req *models.CoursePrimaryKey) (*models.Course, error)
	GetList(ctx context.Context, req *models.GetCourseListRequest) (*models.GetCourseListResponse, error)
	Update(ctx context.Context, req *models.UpdateCourse) (*models.Course, error)
	Delete(ctx context.Context, req *models.CoursePrimaryKey) error
}
type ModuleRepoI interface {
	Create(ctx context.Context, req *models.CreateModule) (*models.Module, error)
	GetByID(ctx context.Context, req *models.ModulePrimaryKey) (*models.Module, error)
	GetList(ctx context.Context, req *models.GetListModuleRequest) (*models.GetListModuleResponse, error)
	Update(ctx context.Context, req *models.UpdateModule) (*models.Module, error)
	Delete(ctx context.Context, req *models.ModulePrimaryKey) error
}
type LessonRepoI interface {
	Create(ctx context.Context, req *models.CreateLesson) (*models.Lesson, error)
	GetByID(ctx context.Context, req *models.LessonPrimaryKey) (*models.Lesson, error)
	GetList(ctx context.Context, req *models.GetListLessonRequest) (*models.GetListLessonResponse, error)
	Update(ctx context.Context, req *models.UpdateLesson) (*models.Lesson, error)
	Delete(ctx context.Context, req *models.LessonPrimaryKey) error
}

type GroupRepoI interface {
	Create(ctx context.Context, req *models.CreateGroup) (*models.Group, error)
	GetByID(ctx context.Context, req *models.GroupPrimaryKey) (*models.Group, error)
	GetList(ctx context.Context, req *models.GetListGroupRequest) (*models.GetListGroupResponse, error)
	Update(ctx context.Context, req *models.UpdateGroup) (*models.Group, error)
	Delete(ctx context.Context, req *models.GroupPrimaryKey) error
}
