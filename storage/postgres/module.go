package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"lms_service/models"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ModuleRepo struct {
	db *pgxpool.Pool
}

func NewModuleRepo(db *pgxpool.Pool) *ModuleRepo {
	return &ModuleRepo{
		db: db,
	}
}

func (u *ModuleRepo) Create(ctx context.Context, req *models.CreateModule) (*models.Module, error) {
	id := uuid.New().String()

	var (
		query = `INSERT INTO "module"(
					id,
					course_id,
					name,
					updated_at
		)VALUES($1,$2,$3,NOW())

		`
	)

	_, err := u.db.Exec(ctx,
		query,
		id,
		req.CourseId,
		req.Name,
	)
	if err != nil {
		return nil, err
	}

	resp, err := u.GetByID(ctx, &models.ModulePrimaryKey{Id: id})
	if err != nil {
		return nil, err
	}

	return resp, nil

}

func (u *ModuleRepo) GetByID(ctx context.Context, req *models.ModulePrimaryKey) (*models.Module, error) {

	var (
		query = `
			SELECT
					"id",
					"course_id",
					"name",
					"created_at",
					"updated_at"
			FROM "module"
			WHERE "id" = $1
		`
	)

	var (
		id         sql.NullString
		course_id  sql.NullString
		name       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&course_id,
		&name,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &models.Module{
		Id:        id.String,
		CourseId:  course_id.String,
		Name:      name.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (u *ModuleRepo) GetList(ctx context.Context, req *models.GetListModuleRequest) (*models.GetListModuleResponse, error) {
	var (
		resp   models.GetListModuleResponse
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		sort   = " ORDER BY created_at DESC"
	)

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.Search) > 0 {
		where += " AND role=" + "'" + req.Search + "'"
	}

	var query = `
		SELECT
			COUNT(*) OVER(),
			id,
			course_id,
			name,
			created_at,
			updated_at	
		FROM "module"
	`

	query += where + sort + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	var i = 1
	for rows.Next() {
		var (
			Id        sql.NullString
			CourseId  sql.NullString
			Name      sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&Id,
			&CourseId,
			&Name,
			&CreatedAt,
			&UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		resp.Modules = append(resp.Modules, &models.Module{
			Id:        Id.String,
			CourseId:  CourseId.String,
			Name:      Name.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})

		i++
	}

	return &resp, nil
}

func (u *ModuleRepo) Update(ctx context.Context, req *models.UpdateModule) (*models.Module, error) {
	query := `
			UPDATE "module"
			SET
				course_id = $1,
				name = $2,
				updated_at = $3
			WHERE id = $4

	`

	_, err := u.db.Exec(ctx, query, req.CourseId, req.Name, time.Now(), req.Id)
	if err != nil {
		return nil, err
	}
	resp, err := u.GetByID(context.Background(), &models.ModulePrimaryKey{Id: req.Id})
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (u *ModuleRepo) Delete(ctx context.Context, req *models.ModulePrimaryKey) error {
	query := `
		DELETE FROM "module"
		WHERE id = $1
	`
	_, err := u.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
