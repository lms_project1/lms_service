package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"lms_service/models"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type GroupRepo struct {
	db *pgxpool.Pool
}

func NewGroupRepo(db *pgxpool.Pool) *GroupRepo {
	return &GroupRepo{
		db: db,
	}
}

func (u *GroupRepo) Create(ctx context.Context, req *models.CreateGroup) (*models.Group, error) {
	id := uuid.New().String()

	var (
		query = `INSERT INTO "group"(
					id,
					name,
					course_id,
					start_date,
					status,
					updated_at
		)VALUES($1,$2,$3,$4,$5,NOW())
		`
	)

	_, err := u.db.Exec(ctx,
		query,
		id,
		req.Name,
		req.CourseId,
		req.StartDate,
		"new",
	)
	if err != nil {
		return nil, err
	}

	resp, err := u.GetByID(ctx, &models.GroupPrimaryKey{Id: id})
	if err != nil {
		return nil, err
	}

	return resp, nil

}

func (u *GroupRepo) GetByID(ctx context.Context, req *models.GroupPrimaryKey) (*models.Group, error) {

	var (
		query = `
			SELECT
					"id",
					"name",
					"course_id",
					"start_date",
					"status",
					"created_at",
					"updated_at"
			FROM "group"
			WHERE "id" = $1
		`
	)

	var (
		id         sql.NullString
		name       sql.NullString
		course_id  sql.NullString
		start_date sql.NullString
		status     sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&course_id,
		&start_date,
		&status,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &models.Group{
		Id:        id.String,
		Name:      name.String,
		CourseId:  course_id.String,
		StartDate: start_date.String,
		Status:    status.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (u *GroupRepo) GetList(ctx context.Context, req *models.GetListGroupRequest) (*models.GetListGroupResponse, error) {
	var (
		resp   models.GetListGroupResponse
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		sort   = " ORDER BY created_at DESC"
	)

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.Search) > 0 {
		where += " AND role=" + "'" + req.Search + "'"
	}

	var query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			course_id,
			start_date,
			status,
			created_at,
			updated_at	
		FROM "group"
	`

	query += where + sort + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	
	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			course_id  sql.NullString
			start_date sql.NullString
			status     sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&name,
			&course_id,
			&start_date,
			&status,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return nil, err
		}

		resp.Groups = append(resp.Groups, &models.Group{
			Id:        id.String,
			Name:      name.String,
			CourseId:  course_id.String,
			StartDate: start_date.String,
			Status:    status.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})

		
	}

	return &resp, nil
}

func (u *GroupRepo) Update(ctx context.Context, req *models.UpdateGroup) (*models.Group, error) {
	query := `
			UPDATE "group"
			SET
				name = $1,
				updated_at = $2
			WHERE id = $3

	`

	_, err := u.db.Exec(ctx, query, 
		req.Name,
		time.Now(), 
		req.Id,
	)
	if err != nil {
		return nil, err
	}
	resp, err := u.GetByID(context.Background(), &models.GroupPrimaryKey{Id: req.Id})
	if err != nil {
		return nil, err
	}
	return resp, nil
}



func (u *GroupRepo) Delete(ctx context.Context, req *models.GroupPrimaryKey) error {
	query := `
		DELETE FROM "group"
		WHERE id = $1
	`
	_, err := u.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
