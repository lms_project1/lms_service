package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"lms_service/models"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *UserRepo {
	return &UserRepo{
		db: db,
	}
}

func (u *UserRepo) Create(ctx context.Context, req *models.CreateUser) (*models.User, error) {
	id := uuid.New().String()

	var (
		query = `INSERT INTO "user"(
					id,
					first_name,
					last_name,
					email,
					password,
					role,
					updated_at
		)VALUES($1,$2,$3,$4,$5,$6,NOW())

		`
	)

	_, err := u.db.Exec(ctx,
		query,
		id,
		req.FirstName,
		req.LastName,
		req.Email,
		req.Password,
		req.Role,
	)
	if err != nil {
		return nil, err
	}

	resp, err := u.GetByID(ctx, &models.UserPrimaryKey{Id: id})
	if err != nil {
		return nil, err
	}

	return resp, nil

}

func (u *UserRepo) GetByID(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error) {

	var (
		query = `
			SELECT
					"id",
					"first_name",
					"last_name",
					"email",
					"password",
					"role",
					"created_at",
					"updated_at"
			FROM "user"
			WHERE "id" = $1
		`
	)

	var (
		id         sql.NullString
		first_name sql.NullString
		last_name  sql.NullString
		email      sql.NullString
		password   sql.NullString
		role       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&first_name,
		&last_name,
		&email,
		&password,
		&role,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &models.User{
		Id:        id.String,
		FirstName: first_name.String,
		LastName:  last_name.String,
		Email:     email.String,
		Password:  password.String,
		Role:      role.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (u *UserRepo) GetList(ctx context.Context, req *models.GetListUserRequest) (*models.GetListUserResponse, error) {
	var (
		resp   models.GetListUserResponse
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		sort   = " ORDER BY created_at DESC"
	)

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.Search) > 0 {
		where += " AND role=" + "'" + req.Search + "'"
	}

	var query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name || ' ' || last_name as name,
			last_name,
			email,
			password,
			role,
			created_at,
			updated_at	
		FROM "user"
	`

	query += where + sort + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	var i = 1
	for rows.Next() {
		var (
			Id        sql.NullString
			FirstName sql.NullString
			LastName  sql.NullString
			Email     sql.NullString
			Password  sql.NullString
			Role      sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&Id,
			&FirstName,
			&LastName,
			&Email,
			&Password,
			&Role,
			&CreatedAt,
			&UpdatedAt,
		)
		if err != nil {
			return nil, err
		}
		
			 
			resp.Users = append(resp.Users, &models.User{
				Id:        Id.String,
				FirstName: FirstName.String,
				LastName:  LastName.String,
				Email:     Email.String,
				Password: 	"",
				Role:      Role.String,
				CreatedAt: CreatedAt.String,
				UpdatedAt: UpdatedAt.String,
			})
		
		i++
	}

	return &resp, nil
}

func (u *UserRepo) Update(ctx context.Context, req *models.UpdateUser) (*models.User, error) {
	query := `
			UPDATE "user"
			SET
				first_name = $1,
				last_name = $2,
				email = $3,
				password = $4,
				updated_at = $5
			WHERE id = $6

	`

	_, err := u.db.Exec(ctx, query, req.FirstName, req.LastName, req.Phone, time.Now(), req.Id)
	if err != nil {
		return nil, err
	}
	resp, err := u.GetByID(context.Background(), &models.UserPrimaryKey{Id: req.Id})
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (u *UserRepo) GetByEmail(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error) {

	var (
		query = `
			SELECT
				"id",
				"first_name",
				"last_name",
				"phone",
				"email",
				"password",
				"role",
				"created_at",
				"updated_at"
			FROM "user"
			WHERE "email" = $1
		`
	)
	fmt.Println(query)
	var (
		id         sql.NullString
		first_name sql.NullString
		last_name  sql.NullString
		phone      sql.NullString
		email      sql.NullString
		password   sql.NullString
		role       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)
	fmt.Println(req.Email)
	err := u.db.QueryRow(ctx, query, req.Email).Scan(
		&id,
		&first_name,
		&last_name,
		&phone,
		&email,
		&password,
		&role,
		&created_at,
		&updated_at,
	)

	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return &models.User{
		Id:        id.String,
		FirstName: first_name.String,
		LastName:  last_name.String,
		Phone:     phone.String,
		Email:     email.String,
		Password:  password.String,
		Role:      role.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (u *UserRepo) Delete(ctx context.Context, req *models.UserPrimaryKey) error {
	query := `
		DELETE FROM "user"
		WHERE id = $1
	`
	_, err := u.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
