package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"lms_service/models"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type LessonRepo struct {
	db *pgxpool.Pool
}

func NewLessonRepo(db *pgxpool.Pool) *LessonRepo {
	return &LessonRepo{
		db: db,
	}
}

func (u *LessonRepo) Create(ctx context.Context, req *models.CreateLesson) (*models.Lesson, error) {
	id := uuid.New().String()

	var (
		query = `INSERT INTO "lesson"(
					id,
					module_id,
					name,
					updated_at
		)VALUES($1,$2,$3,NOW())

		`
	)

	_, err := u.db.Exec(ctx,
		query,
		id,
		req.ModuleId,
		req.Name,
	)
	if err != nil {
		return nil, err
	}

	resp, err := u.GetByID(ctx, &models.LessonPrimaryKey{Id: id})
	if err != nil {
		return nil, err
	}

	return resp, nil

}

func (u *LessonRepo) GetByID(ctx context.Context, req *models.LessonPrimaryKey) (*models.Lesson, error) {

	var (
		query = `
			SELECT
					"id",
					"module_id",
					"name",
					"created_at",
					"updated_at"
			FROM "lesson"
			WHERE "id" = $1
		`
	)

	var (
		id         sql.NullString
		ModuleId  sql.NullString
		name       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&ModuleId,
		&name,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &models.Lesson{
		Id:        id.String,
		ModuleId:  ModuleId.String,
		Name:      name.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (u *LessonRepo) GetList(ctx context.Context, req *models.GetListLessonRequest) (*models.GetListLessonResponse, error) {
	var (
		resp   models.GetListLessonResponse
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		sort   = " ORDER BY created_at DESC"
	)

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.Search) > 0 {
		where += " AND role=" + "'" + req.Search + "'"
	}

	var query = `
		SELECT
			COUNT(*) OVER(),
			id,
			module_id,
			name,
			created_at,
			updated_at	
		FROM "lesson"
	`

	query += where + sort + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	var i = 1
	for rows.Next() {
		var (
			Id        sql.NullString
			ModuleId  sql.NullString
			Name      sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&Id,
			&ModuleId,
			&Name,
			&CreatedAt,
			&UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		resp.Lessons = append(resp.Lessons, &models.Lesson{
			Id:        Id.String,
			ModuleId:  ModuleId.String,
			Name:      Name.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})

		i++
	}

	return &resp, nil
}

func (u *LessonRepo) Update(ctx context.Context, req *models.UpdateLesson) (*models.Lesson, error) {
	query := `
			UPDATE "lesson"
			SET
				module_id = $1,
				name = $2,
				updated_at = $3
			WHERE id = $4

	`

	_, err := u.db.Exec(ctx, query, req.ModuleId, req.Name, time.Now(), req.Id)
	if err != nil {
		return nil, err
	}
	resp, err := u.GetByID(context.Background(), &models.LessonPrimaryKey{Id: req.Id})
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (u *LessonRepo) Delete(ctx context.Context, req *models.LessonPrimaryKey) error {
	query := `
		DELETE FROM "lesson"
		WHERE id = $1
	`
	_, err := u.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
