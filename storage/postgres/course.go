package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"lms_service/models"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type CourseRepo struct {
	db *pgxpool.Pool
}

func NewCourseRepo(db *pgxpool.Pool) *CourseRepo {
	return &CourseRepo{
		db: db,
	}
}

func (c *CourseRepo) Create(ctx context.Context, req *models.CreateCourse) (*models.Course, error) {
	id := uuid.New().String()
	query := `
		INSERT INTO course(
			id,
			teacher_id,
			name,
			image,
			category,
			description,
			price,
			start_time,
			lesson_duration,
			number_of_lessons_per_week,
			updated_at
		)VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,NOW())
		`

	_, err := c.db.Exec(ctx, query, id, req.TeacherId, req.Name, req.Image, req.Category, req.Description, req.Price, req.Start_time, req.LessonDuration, req.NumberOfLessonsPerWeek)
	if err != nil {
		return nil, err
	}
	resp, err := c.GetByID(ctx, &models.CoursePrimaryKey{Id: id})
	if err != nil {
		return nil, err
	}
	return resp, nil

}

func (c *CourseRepo) Update(ctx context.Context, req *models.UpdateCourse) (*models.Course, error) {
	query := `
		UPDATE course
		SET
			name = $1,
			image = $2,
			category = $3,
			description = $4,
			price = $5,
			start_time = $6,
			lesson_duration = $7,
			number_of_lessons_per_week = $8,
			updated_at = NOW()
		WHERE id = $9
		`

	_, err := c.db.Exec(ctx, query, req.Name, req.Image, req.Category, req.Description, req.Price, req.Start_time, req.LessonDuration, req.NumberOfLessonsPerWeek)
	if err != nil {
		return nil, err
	}
	resp, err := c.GetByID(ctx, &models.CoursePrimaryKey{Id: req.Id})
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *CourseRepo) GetByID(ctx context.Context, req *models.CoursePrimaryKey) (*models.Course, error) {
	var (
		query = `
		SELECT
			id,
			teacher_id,
			name,
			image,
			category,
			description,
			price,
			start_time,
			lesson_duration,
			number_of_lessons_per_week,
			created_at,
			updated_at
		FROM course
		WHERE id = $1
		`
	)

	var (
		id                     sql.NullString
		teacherId              sql.NullString
		name                   sql.NullString
		image                  sql.NullString
		category               sql.NullString
		description            sql.NullString
		price                  sql.NullFloat64
		start_time             sql.NullString
		lesson_duration        sql.NullInt64
		numberOfLessonsPerWeek sql.NullInt64
		created_at             sql.NullString
		update_at              sql.NullString
	)

	err := c.db.QueryRow(ctx, query, req.Id).Scan(
		&id, 
		&teacherId, 
		&name, 
		&image, 
		&category, 
		&description, 
		&price, 
		&start_time, 
		&lesson_duration, 
		&numberOfLessonsPerWeek, 
		&created_at, 
		&update_at,
	)
	if err != nil {
		return nil, err
	}

	return &models.Course{
		Id:                     id.String,
		TeacherId:              teacherId.String,
		Name:                   name.String,
		Image:                  image.String,
		Category:               category.String,
		Description:            description.String,
		Price:                  price.Float64,
		Start_time:             start_time.String,
		LessonDuration:         lesson_duration.Int64,
		NumberOfLessonsPerWeek: numberOfLessonsPerWeek.Int64,
		CreatedAt:              created_at.String,
		UpdatedAt:              update_at.String,
	}, nil
}

func (c *CourseRepo) GetList(ctx context.Context, req *models.GetCourseListRequest) (*models.GetCourseListResponse, error) {
	var (
		resp   models.GetCourseListResponse
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		sort   = " ORDER BY created_at DESC"
	)

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.Search) > 0 {
		where += " AND role=" + "'" + req.Search + "'"
	}

	var (
		query = `
		SELECT
			COUNT(*) OVER(),
			id,
			teacher_id,
			name,
			image,
			category,
			description,
			price,
			start_time,
			lesson_duration,
			number_of_lessons_per_week,
			created_at,
			updated_at
		FROM course
		`
	)
	query += where + sort + offset + limit
	fmt.Println(query)
	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var (
			id                     sql.NullString
			teacherId              sql.NullString
			name                   sql.NullString
			image                  sql.NullString
			category               sql.NullString
			description            sql.NullString
			price                  sql.NullFloat64
			start_time             sql.NullString
			lesson_duration        sql.NullInt64
			numberOfLessonsPerWeek sql.NullInt64
			created_at             sql.NullString
			update_at              sql.NullString
		)

		err := rows.Scan(&resp.Count,&id, &teacherId, &name, &image, &category, &description, &price, &start_time, &lesson_duration, &numberOfLessonsPerWeek, &created_at, &update_at)
		if err != nil {
			return nil, err
		}

		resp.Courses = append(resp.Courses, &models.Course{
			Id:                     id.String,
			TeacherId:              teacherId.String,
			Name:                   name.String,
			Image:                  image.String,
			Category:               category.String,
			Description:            description.String,
			Price:                  price.Float64,
			Start_time:             start_time.String,
			LessonDuration:         lesson_duration.Int64,
			NumberOfLessonsPerWeek: numberOfLessonsPerWeek.Int64,
			CreatedAt:              created_at.String,
			UpdatedAt:              update_at.String,
		})
	}

	return &resp, nil
}

func (c *CourseRepo) Delete(ctx context.Context, req *models.CoursePrimaryKey) error {
	_, err := c.db.Exec(ctx, `DELETE FROM course WHERE id = $1`, req.Id)
	if err != nil {
		return err
	}
	return nil
}
