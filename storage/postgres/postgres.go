package postgres

import (
	"context"
	"fmt"
	"lms_service/config"
	"lms_service/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db     *pgxpool.Pool
	otp    *OTPRepo
	user   *UserRepo
	course *CourseRepo
	group  *GroupRepo
	module *ModuleRepo
	lesson *LessonRepo
}

func (s *Store) OTP() storage.OTPRepoI {

	if s.otp == nil {
		s.otp = NewOTPRepo(s.db)
	}

	return s.otp
}

func NewConnectionPostgres(cfg *config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(

		fmt.Sprintf(
			"host=%s user=%s dbname=%s password=%s port=%s ",
			cfg.PostgresHost,
			cfg.PostgresUser,
			cfg.PostgresDatabase,
			cfg.PostgresPassword,
			cfg.PostgresPort,
		),
	)
	if err != nil {
		return nil, err
	}
	config.MaxConns = cfg.PostgresMaxConnection
	pgxpool, err := pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		panic(err)
	}
	return &Store{
		db: pgxpool,
	}, nil
}

func (s *Store) User() storage.UserRepoI {

	if s.user == nil {
		s.user = NewUserRepo(s.db)
	}

	return s.user
}

func (s *Store) Course() storage.CourseRepoI {

	if s.course == nil {
		s.course = NewCourseRepo(s.db)
	}

	return s.course
}
func (s *Store) Module() storage.ModuleRepoI {

	if s.module == nil {
		s.module = NewModuleRepo(s.db)
	}

	return s.module
}
func (s *Store) Lesson() storage.LessonRepoI {

	if s.lesson == nil {
		s.lesson = NewLessonRepo(s.db)
	}

	return s.lesson
}

func (s *Store) Group() storage.GroupRepoI {

	if s.group == nil {
		s.group = NewGroupRepo(s.db)
	}

	return s.group
}