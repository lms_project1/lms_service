CREATE TABLE "user"(
    "id" UUID PRIMARY KEY,
    "first_name" VARCHAR(45) NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "phone" VARCHAR(15),
    "email" VARCHAR NOT NULL UNIQUE,
    "password" VARCHAR NOT NULL,
    "role" varchar(100) NOT NULL CHECK (role IN('admin','teacher','student')),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE  "verification_code"(
    "email" VARCHAR NOT NULL,
    "code" INTEGER NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "expired_at" TIMESTAMP 
    );

CREATE TABLE "course"(
    "id" UUID PRIMARY KEY,
    "teacher_id" UUID REFERENCES "user"(id) ON DELETE CASCADE,
    "name" VARCHAR(100) NOT NULL,
    "image" VARCHAR,
    "category" VARCHAR,
    "description" TEXT,
    "price" NUMERIC,
    "lesson_duration" INTEGER,
    "number_of_lessons_per_week" INTEGER,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "group"(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL,
    "course_id" UUID REFERENCES "course"(id) ON DELETE CASCADE,
    "start_date" TIMESTAMP,
    "status" VARCHAR(100) NOT NULL CHECK (status IN('active','inactive','new')),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
)


CREATE TABLE "module"(
    "id" UUID PRIMARY KEY,
    "course_id" UUID REFERENCES "course"(id) ON DELETE CASCADE,
    "name" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP

);


CREATE TABLE "lesson"(
    "id" UUID PRIMARY KEY,
    "module_id" UUID REFERENCES "module"(id) ON DELETE CASCADE,
    "name" VARCHAR(100) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);




CREATE TABLE "enrolled_student"(
    "student_id" UUID REFERENCES "user"("id") ON DELETE CASCADE,
    "group_id" UUID REFERENCES "group"("id") ON DELETE CASCADE
)


INSERT INTO "user"("id","first_name","last_name","email","password","role")
VALUES('64e0abc7-668f-4a84-b48a-327d2e424259','admin','admin','admin@gmail.com','$2a$07$LqVCXtswkJ/TdlTTSPJwvuuCe.pQqR35JO.1GoLtr9LrDV9ur85NS','admin');
