package models

type Course struct {
	Id                     string  `json:"id"`
	TeacherId              string  `json:"teacher_id"`
	Name                   string  `json:"name"`
	Image                  string  `json:"image"`
	Category               string  `json:"category"`
	Description            string  `json:"description"`
	Price                  float64 `json:"price"`
	Start_time             string  `json:"start_time"`
	LessonDuration         int64   `json:"lesson_duration"`
	NumberOfLessonsPerWeek int64   `json:"number_of_lessons_per_week"`
	CreatedAt              string  `json:"created_at"`
	UpdatedAt              string  `json:"updated_at"`
}

type CreateCourse struct {
	TeacherId              string  `json:"teacher_id"`
	Name                   string  `json:"name"`
	Image                  string  `json:"image"`
	Category               string  `json:"category"`
	Description            string  `json:"description"`
	Price                  float64 `json:"price"`
	Start_time             string  `json:"start_time"`
	LessonDuration         int64   `json:"lesson_duration"`
	NumberOfLessonsPerWeek int64   `json:"number_of_lessons_per_week"`
}

type CoursePrimaryKey struct {
	Id string `json:"id"`
}

type UpdateCourse struct {
	Id                     string  `json:"id"`
	Name                   string  `json:"name"`
	Image                  string  `json:"image"`
	Category               string  `json:"category"`
	Description            string  `json:"description"`
	Price                  float64 `json:"price"`
	Start_time             string  `json:"start_time"`
	LessonDuration         int64   `json:"lesson_duration"`
	NumberOfLessonsPerWeek int64   `json:"number_of_lessons_per_week"`
}
type GetCourseListRequest struct {
	Offset int64  `json:"offset"`
	Limit  int64  `json:"limit"`
	Search string `json:"search"`
	Query  string `json:"query"`
}

type GetCourseListResponse struct {
	Count   int64     `json:"count"`
	Courses []*Course `json:"courses"`
}
