package models

type Lesson struct {
	Id        string `json:"id"`
	ModuleId  string `json:"module_id"`
	Name      string `json:"name"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type CreateLesson struct {
	ModuleId string `json:"module_id"`
	Name     string `json:"name"`
}

type LessonPrimaryKey struct {
	Id string `json:"id"`
}

type UpdateLesson struct {
	Id       string `json:"id"`
	ModuleId string `json:"module_id"`
	Name     string `json:"name"`
}

type GetListLessonRequest struct {
	Offset int64  `json:"offset"`
	Limit  int64  `json:"limit"`
	Search string `json:"search"`
	Query  string `json:"query"`
}

type GetListLessonResponse struct {
	Count   int64     `json:"count"`
	Lessons []*Lesson `json:"data"`
}
