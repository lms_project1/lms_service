package models

type Module struct {
	Id        string `json:"id"`
	CourseId  string `json:"course_id"`
	Name      string `json:"name"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type CreateModule struct {
	CourseId string `json:"course_id"`
	Name     string `json:"name"`
}

type ModulePrimaryKey struct {
	Id string `json:"id"`
}

type UpdateModule struct {
	Id       string `json:"id"`
	CourseId string `json:"course_id"`
	Name     string `json:"name"`
}

type GetListModuleRequest struct {
	Offset int64  `json:"offset"`
	Limit  int64  `json:"limit"`
	Search string `json:"search"`
	Query  string `json:"query"`
}

type GetListModuleResponse struct {
	Count   int64     `json:"count"`
	Modules []*Module `json:"data"`
}
