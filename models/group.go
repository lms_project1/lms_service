package models

type Group struct{
	Id string `json:"id"`
	Name string `json:"name"`
	CourseId string `json:"course_id"`
	StartDate string `json:"start_date"`
	Status string `json:"status"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}


type CreateGroup struct{
	Name string `json:"name"`
	CourseId string `json:"course_id"`
	StartDate string `json:"start_date"`
}

type GroupPrimaryKey struct{
	Id string `json:"id"`
}

type UpdateGroup struct{
	Id string `json:"id"`
	Name string `json:"name"`
}

type GetListGroupRequest struct{
	Offset int64 `json:"offset"`
	Limit int64 `json:"limit"`
	Search string `json:"search"`
}

type GetListGroupResponse struct{
	Count int64 `json:"count"`
	Groups []*Group `json:"data"`
}

