package main

import (
	"lms_service/config"
	"lms_service/handler"
	"lms_service/storage/postgres"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	var cfg = config.Load()
	strg, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}
	handler := handler.NewHandler(&cfg, strg)
	r := gin.Default()
	r.Use(CORSMiddleware())
	r.POST("api/v1/reports",handler.GenerateExcelStudent )
	r.POST("api/v1/upload",handler.UploadFile)
	//Auth routes
	r.POST("api/v1/login", handler.Login)
	r.POST("api/v1/register", handler.CreateStudent)
	r.POST("api/v1/sendcode", handler.SendOTP)
	r.POST("api/v1/verify", handler.Verify)
	r.PUT("api/v1/changepassword", handler.ChangePassword)
	r.GET("api/v1/userbyemail/:email", handler.GetUserByEmail)

	//Student routes
	r.POST("api/v1/students", handler.CreateStudent)
	r.GET("api/v1/students/:id",  handler.GetUserByID)
	r.GET("api/v1/students",  handler.GetStudentList)
	r.DELETE("api/v1/students/:id",  handler.DeleteUser)

	//Teacher routes
	r.POST("api/v1/teacher",  handler.CreateTeacher)
	r.GET("api/v1/teachers/:id",  handler.GetUserByID)
	r.GET("api/v1/teachers",  handler.GetTeacherList)
	r.DELETE("api/v1/teachers/:id",  handler.DeleteUser)

	//Course routes
	r.POST("api/v1/courses",  handler.CreateCourse)
	r.GET("api/v1/courses/:id", handler.GetCourseByID)
	r.GET("api/v1/courses",  handler.GetCourseList)
	r.PUT("api/v1/courses",  handler.UpdateCourse)
	r.DELETE("api/v1/courses/:id",  handler.DeleteCourse)

	//Module routes
	r.POST("api/v1/modules",  handler.CreateModule)
	r.GET("api/v1/modules/:id", handler.GetModuleByID)
	r.GET("api/v1/modules",  handler.GetModuleList)
	r.PUT("api/v1/modules",  handler.UpdateModule)
	r.DELETE("api/v1/modules/:id",  handler.DeleteModule)

	//Lesson routes
	r.POST("api/v1/lessons",  handler.CreateLesson)
	r.GET("api/v1/lessons/:id", handler.GetLessonByID)
	r.GET("api/v1/lessons",  handler.GetLessonList)
	r.PUT("api/v1/lessons",  handler.UpdateLesson)
	r.DELETE("api/v1/lessons/:id",  handler.DeleteLesson)

	//Group routes
	r.POST("api/v1/groups",  handler.CreateGroup)
	r.GET("api/v1/groups/:id", handler.GetGroupByID)
	r.GET("api/v1/groups",  handler.GetGroupList)
	r.PUT("api/v1/groups",  handler.UpdateGroup)
	r.DELETE("api/v1/groups/:id",  handler.DeleteGroup)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if err := r.Run(":" + port); err != nil {
		log.Panicf("error: %s", "panic")
	}
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE, PATCH")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
