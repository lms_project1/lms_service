package handler

import (
	"errors"
	"net/http"
	"lms_service/pkg/helper"

	"github.com/gin-gonic/gin"
)



func (h *Handler) AuthMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {

		value := c.GetHeader("Authorization")

		info, err := helper.ParseClaims(value, h.cfg.SecretKey)
		if err != nil {
			c.AbortWithError(http.StatusForbidden, err)
			return
		}
		
		c.Set("Auth", info)

		c.Next()
	}
}
func (h *Handler) CheckRoleAdminMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {

		// get user id from token
		info, exists := c.Get("Auth")
		if !exists {
			c.AbortWithError(http.StatusForbidden, errors.New("invalid info  token"))
			return
		}

		userData := info.(helper.TokenInfo)
		

		if userData.UserType != "admin" {
			c.AbortWithError(http.StatusForbidden, errors.New("client type not permittible"))
			c.JSON(http.StatusForbidden, map[string]interface{}{
				"role": "client type not permittible",
			})
			return
		}

		c.Next()
	}
}

