package handler

import (
	"context"
	"fmt"
	"lms_service/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (h *Handler) CreateModule(c *gin.Context) {
	var module models.CreateModule
	err := c.ShouldBindJSON(&module)
	fmt.Printf("Module: %v", module)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	resp, err := h.strg.Module().Create(context.Background(), &module)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}
func (h *Handler) GetModuleByID(c *gin.Context) {
	id := c.Param("id")
	resp, err := h.strg.Module().GetByID(context.Background(), &models.ModulePrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, resp)
}

func (h *Handler) GetModuleList(c *gin.Context) {
	var req models.GetListModuleRequest
	offset := c.Query("offset")
	limit := c.Query("limit")
	req.Offset, _ = strconv.ParseInt(offset, 10, 64)
	req.Limit, _ = strconv.ParseInt(limit, 10, 64)
	fmt.Println(req)

	resp, err := h.strg.Module().GetList(context.Background(), &req)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) UpdateModule(c *gin.Context) {
	var module models.UpdateModule
	err := c.ShouldBindJSON(&module)
	if err != nil {
		c.JSON(500, "failed to marshall json file")
		return
	}
	resp, err := h.strg.Module().Update(context.Background(), &module)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) DeleteModule(c *gin.Context) {
	id := c.Param("id")
	err := h.strg.Module().Delete(context.Background(), &models.ModulePrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "success")
}
