package handler

import (
	"context"
	"fmt"
	"lms_service/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (h *Handler) CreateGroup(c *gin.Context) {
	var group models.CreateGroup
	err := c.ShouldBindJSON(&group)
	fmt.Printf("Group: %v", group)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	resp, err := h.strg.Group().Create(context.Background(), &group)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}
func (h *Handler) GetGroupByID(c *gin.Context) {
	id := c.Param("id")
	resp, err := h.strg.Group().GetByID(context.Background(), &models.GroupPrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, resp)
}

func (h *Handler) GetGroupList(c *gin.Context) {
	var req models.GetListGroupRequest
	offset := c.Query("offset")
	limit := c.Query("limit")
	req.Offset, _ = strconv.ParseInt(offset, 10, 64)
	req.Limit, _ = strconv.ParseInt(limit, 10, 64)
	fmt.Println(req)

	resp, err := h.strg.Group().GetList(context.Background(), &req)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) UpdateGroup(c *gin.Context) {
	var group models.UpdateGroup
	err := c.ShouldBindJSON(&group)
	if err != nil {
		c.JSON(500, "failed to marshall json file")
		return
	}
	resp, err := h.strg.Group().Update(context.Background(), &group)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) DeleteGroup(c *gin.Context) {
	id := c.Param("id")
	err := h.strg.Group().Delete(context.Background(), &models.GroupPrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "success")
}
