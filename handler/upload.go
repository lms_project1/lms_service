package handler

import (
	"context"
	"fmt"
	"io"
	"os"

	firebase "firebase.google.com/go"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"google.golang.org/api/option"
)

func (h *Handler) UploadFile(c *gin.Context) {
	file, err := c.MultipartForm()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	lessonFile, err := file.File["file"][0].Open()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	id := uuid.New().String()
	tempFile, err := os.Create(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	_, err = io.Copy(tempFile, lessonFile)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	defer tempFile.Close()

	godotenv.Load("app.env")

	opt := option.WithCredentialsFile("serviceAccountKey.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	client, err := app.Storage(context.TODO())
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	bucketHandle, err := client.Bucket(os.Getenv("BUCKET_NAME"))
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	f, err := os.Open(tempFile.Name())
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	objectHandle := bucketHandle.Object(tempFile.Name())

	writer := objectHandle.NewWriter(context.Background())

	idd := uuid.New().String()
	writer.ObjectAttrs.Metadata = map[string]string{"firebaseStorageDownloadTokens": idd}

	defer writer.Close()

	if _, err := io.Copy(writer, f); err != nil {
		c.JSON(500, err.Error())
		return
	}

	url := fmt.Sprintf("https://firebasestorage.googleapis.com/v0/b/lms-service-415606.appspot.com/o/%s?alt=media&token=%s", id, idd)
	os.Remove(tempFile.Name())

	c.JSON(200, url)

}
