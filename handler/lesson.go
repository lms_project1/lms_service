package handler

import (
	"context"
	"fmt"
	"lms_service/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (h *Handler) CreateLesson(c *gin.Context) {
	var lesson models.CreateLesson
	err := c.ShouldBindJSON(&lesson)
	fmt.Printf("Lesson: %v", lesson)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	resp, err := h.strg.Lesson().Create(context.Background(), &lesson)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}
func (h *Handler) GetLessonByID(c *gin.Context) {
	id := c.Param("id")
	resp, err := h.strg.Lesson().GetByID(context.Background(), &models.LessonPrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, resp)
}

func (h *Handler) GetLessonList(c *gin.Context) {
	var req models.GetListLessonRequest
	offset := c.Query("offset")
	limit := c.Query("limit")
	req.Offset, _ = strconv.ParseInt(offset, 10, 64)
	req.Limit, _ = strconv.ParseInt(limit, 10, 64)
	fmt.Println(req)

	resp, err := h.strg.Lesson().GetList(context.Background(), &req)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) UpdateLesson(c *gin.Context) {
	var lesson models.UpdateLesson
	err := c.ShouldBindJSON(&lesson)
	if err != nil {
		c.JSON(500, "failed to marshall json file")
		return
	}
	resp, err := h.strg.Lesson().Update(context.Background(), &lesson)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) DeleteLesson(c *gin.Context) {
	id := c.Param("id")
	err := h.strg.Lesson().Delete(context.Background(), &models.LessonPrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "success")
}
