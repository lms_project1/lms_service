package handler

import (
	"context"
	"fmt"
	"lms_service/config"
	"lms_service/models"
	"lms_service/pkg/helper"
	"lms_service/storage/postgres"
	"math/rand"
	"net/http"
	"net/smtp"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func (h *Handler) Login(c *gin.Context) {
	var cfg = config.Load()
	strg, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}
	email := c.Query("email")
	password := c.Query("password")

	user, err := strg.User().GetByEmail(context.Background(), &models.UserPrimaryKey{Email: email})
	if err != nil {
		c.JSON(400, err)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		c.JSON(404, "wrong login or password")
		return
	}

	user.Password = ""

	data := map[string]interface{}{
		"Id":   user.Id,
		"role": user.Role,
	}

	token, err := helper.GenerateJWT(data, config.TimeExpiredAt, h.cfg.SecretKey)
	if err != nil {
		c.JSON(404, "token error")
		return
	}
	fmt.Println(token)

	c.JSON(200, models.LoginResponse{UserData: user, Token: token})
}

func (h *Handler) SendOTP(c *gin.Context) {

	var email = c.Query("email")
	var createforget models.ForgetPasswords
	from := "ilyos1122@gmail.com"
	password := "opsnagvrxqmvlhpd"

	to := []string{
		email,
	}

	smtpHost := "smtp.gmail.com"
	smtpPort := "587"
	msg := rand.Intn(90000) + rand.Intn(10000)
	code := strconv.Itoa(msg)
	message := []byte(code)

	auth := smtp.PlainAuth("", from, password, smtpHost)
	stringmsg := fmt.Sprintf("%v", msg)
	createforget.Code = stringmsg
	createforget.Email = email

	resp, err := h.strg.OTP().GetByEmail(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	if err != nil && err.Error() != "no rows in result set" {
		c.JSON(500, err.Error())
		return
	}
	if resp == nil {
		err = h.strg.OTP().Create(context.Background(), &createforget)
		if err != nil {
			c.JSON(500, resp)
			return
		}
		err = smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
		if err != nil {
			c.JSON(404, err.Error())
			return
		}
		c.JSON(200, email)
		return
	} else {
		err = h.strg.OTP().Delete(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
		if err != nil {
			c.JSON(500, err.Error())
			return
		}
		err = smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
		if err != nil {
			c.JSON(404, err.Error())
			return
		}
		c.JSON(200, email)
		return

	}

}

func (h *Handler) Verify(c *gin.Context) {
	email := c.Query("email")
	code := c.Query("code")

	resp, err := h.strg.OTP().GetByEmail(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	if resp.Code != code {
		c.JSON(http.StatusBadRequest, "wrong code")
		return
	}
	err = h.strg.OTP().Delete(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var req models.VerifyOTPRequest

	user, err := h.strg.User().GetByEmail(c, &models.UserPrimaryKey{
		Email: req.Email,
	})
	if err != nil {
		if err.Error() == "no rows in result set" {
			resp := &models.AccessToken{
				Token:     "",
				UserFound: false,
				FirstName: "",
				LastName:  "",
				Email:     req.Email,
				UserId:    "",
			}
			c.JSON(http.StatusOK, resp)
			return
		} else {
			c.JSON(404, err.Error())
			return
		}
	}

	//// generate token for user
	data := map[string]interface{}{
		"user_id": user.Id,
		"role":    user.Role,
	}

	token, err := helper.GenerateJWT(data, config.TimeExpiredAt, h.cfg.SecretKey)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	respp := &models.AccessToken{
		Token:     token,
		UserFound: true,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     req.Email,
		UserId:    user.Id,
	}

	c.JSON(200, respp)
}

func (h *Handler) ChangePassword(c *gin.Context) {

	email := c.Query("email")
	newPassword := c.Query("password")

	user, err := h.strg.User().GetByEmail(context.Background(), &models.UserPrimaryKey{Email: email})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	resp, err := h.strg.User().Update(context.Background(), &models.UpdateUser{
		Id:        user.Id,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Password:  newPassword,
	})
	if err != nil {
		c.JSON(500, err.Error())
	}

	c.JSON(200, resp)
}
