package handler

import (
	"context"
	"fmt"
	"lms_service/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (h *Handler) CreateCourse(c *gin.Context) {
	var course models.CreateCourse
	err := c.ShouldBindJSON(&course)
	fmt.Printf("course: %v", course)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	resp, err := h.strg.Course().Create(context.Background(), &course)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}
func (h *Handler) GetCourseByID(c *gin.Context) {
	id := c.Param("id")
	resp, err := h.strg.Course().GetByID(context.Background(), &models.CoursePrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, resp)
}

func (h *Handler) GetCourseList(c *gin.Context) {
	var req models.GetCourseListRequest
	offset := c.Query("offset")
	limit := c.Query("limit")
	req.Offset, _ = strconv.ParseInt(offset, 10, 64)
	req.Limit, _ = strconv.ParseInt(limit, 10, 64)
	fmt.Println(req)

	resp, err := h.strg.Course().GetList(context.Background(), &req)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) UpdateCourse(c *gin.Context) {
	var course models.UpdateCourse
	err := c.ShouldBindJSON(&course)
	if err != nil {
		c.JSON(500, "failed to marshall json file")
		return
	}
	resp, err := h.strg.Course().Update(context.Background(), &course)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)
}

func (h *Handler) DeleteCourse(c *gin.Context) {
	id := c.Param("id")
	err := h.strg.Course().Delete(context.Background(), &models.CoursePrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "success")
}
